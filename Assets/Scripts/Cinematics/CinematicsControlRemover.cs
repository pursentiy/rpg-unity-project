﻿using RPG.Control;
using RPG.Core;
using UnityEngine;
using UnityEngine.Playables;

namespace PRG.Cinematics
{
	public class CinematicsControlRemover : MonoBehaviour
	{
		GameObject player;

		private void Start()
		{
			GetComponent<PlayableDirector>().played += DisableControl;
			GetComponent<PlayableDirector>().stopped += EnableControl;
			player = GameObject.FindGameObjectWithTag("Player");
		}

		void DisableControl(PlayableDirector pd)
		{
			player.GetComponent<ActionScheduler>().CancelCurrentAction();
			player.GetComponent<PlayerController>().enabled = false;
		}

		void EnableControl(PlayableDirector pd)
		{
			player.GetComponent<PlayerController>().enabled = true;
		}

	}
}