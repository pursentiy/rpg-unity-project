﻿using UnityEngine;
using UnityEngine.Playables;

namespace RPG.Cinematics
{
	public class CinematicTrigger : MonoBehaviour
	{
		bool alreadyTriggered = false;

		private void OnTriggerEnter(Collider other)
		{
			if (alreadyTriggered) return;
			if(other.tag == "Player")
			{
				GetComponent<PlayableDirector>().Play();
				alreadyTriggered = true;
			}
		}
	}
}
