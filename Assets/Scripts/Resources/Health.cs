﻿using System;
using RPG.Core;
using RPG.Saving;
using RPG.Stats;
using UnityEngine;

namespace RPG.Resources
{
	public class Health : MonoBehaviour, ISaveable
    {
		[SerializeField] float regenerationPercentage = 70f;
		float healthPoints = -1f;


		bool isDead = false;
		float maxHealth;

		private void Start()
		{
			GetComponent<BaseStats>().onLevelUp += RegenerateHealth;
			if(healthPoints < 0)
			{
				healthPoints = GetComponent<BaseStats>().GetStat(Stat.Health);
				maxHealth = healthPoints;
			}
		}

		public bool IsDead()
		{
			return isDead;
		}

		public void TakeDamage(GameObject instigator, float damage)
		{
			Debug.Log(gameObject.name + " took damage: " + damage);
			healthPoints = Mathf.Max(healthPoints - damage, 0);
			if(healthPoints == 0 && !isDead)
			{
				AwardExperience(instigator);
				Die();
			}
		}

		private void AwardExperience(GameObject instigator)
		{
			Experience experience = instigator.GetComponent<Experience>();
			if (experience == null) return;
			experience.GainExperience(GetComponent<BaseStats>().GetStat(Stat.ExperienceReward));
		}

		public float GetHealthPoints()
		{
			return healthPoints;
		}

		public float GetMaxHealthPoints()
		{
			return GetComponent<BaseStats>().GetStat(Stat.Health);
		}

		public float GetPercentage()
		{
			return Mathf.RoundToInt(healthPoints * 100 / maxHealth);
		}

		private void Die()
		{
			isDead = true;
			GetComponent<Animator>().SetTrigger("die");
			GetComponent<ActionScheduler>().CancelCurrentAction();
		}

		private void RegenerateHealth()
		{
			float regenHealthPoints = GetComponent<BaseStats>().GetStat(Stat.Health) * ( regenerationPercentage / 100);
			healthPoints = Mathf.Max(healthPoints, regenHealthPoints);
		}

        public object CaptureState()
        {
            return healthPoints;
        }

        public void RestoreState(object state)
        {
            healthPoints = (float)state;

            if(healthPoints == 0)
            {
                Die();
            }
        }
    }
}
