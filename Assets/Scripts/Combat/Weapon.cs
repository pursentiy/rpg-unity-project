﻿using RPG.Core;
using RPG.Resources;
using UnityEngine;

namespace RPG.Combat
{
	[CreateAssetMenu(fileName ="Weapon", menuName = "Weapons/Make new Weapon", order = 0)]
	public class Weapon : ScriptableObject
	{
		[SerializeField] float weaponDamage;
		[SerializeField] float weaponRange = 2f;
		[SerializeField] float percentageBonus = 0;
		[SerializeField] AnimatorOverrideController animatorOverride = null;
		[SerializeField] GameObject weaponPrefab;
		[SerializeField] bool isRightHanded = true;
		[SerializeField] Projectile projectile = null;

		const string weaponName = "Weapon";

		public void Spawn(Transform rightHand, Transform leftHand, Animator animator)
		{
			DestroyOldWeapon(rightHand, leftHand);
			if (weaponPrefab != null)
			{
				GameObject weapon = Instantiate(weaponPrefab, GetHandTransform(rightHand, leftHand));
				weapon.name = weaponName;
			}

			var overrideController = animator.runtimeAnimatorController as AnimatorOverrideController;
			if (animatorOverride != null)
			{
				animator.runtimeAnimatorController = animatorOverride;
			}
			else if(overrideController != null)
			{
					animator.runtimeAnimatorController = overrideController.runtimeAnimatorController;
			}
		}

		private void DestroyOldWeapon(Transform rightHand, Transform leftHand)
		{
			Transform oldWeapon = rightHand.Find(weaponName);
			if(oldWeapon == null)
			{
				oldWeapon = leftHand.Find(weaponName);
			}
			if (oldWeapon == null) return;

			oldWeapon.name = "DESTROYING";
			Destroy(oldWeapon.gameObject);
		}

		public bool HasProjectile()
		{
			return projectile != null;
		}

		public void LaunchProjectile(Transform rightHand, Transform leftHand, Health target, GameObject instigator, float calculatedDamage)
		{
			Projectile projectileInstance = 
				Instantiate(projectile, GetHandTransform(rightHand, leftHand).position, Quaternion.identity);
			projectileInstance.SetTarget(target, instigator, calculatedDamage);
		}

		public float GetDamage()
		{
			return weaponDamage;
		}

		public float GetRange()
		{
			return weaponRange;
		}

		public float GetPercentageBonus()
		{
			return percentageBonus;
		}

		private Transform GetHandTransform(Transform rightHand, Transform leftHand)
		{
			Transform handTransform;
			if (isRightHanded) handTransform = rightHand;
			else handTransform = leftHand;
			return handTransform;
		}

	}
}

