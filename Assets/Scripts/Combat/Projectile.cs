﻿using RPG.Resources;
using UnityEngine;

namespace RPG.Core
{
	public class Projectile : MonoBehaviour
	{

		[SerializeField] float arrowSpeed = 1f;
		[SerializeField] bool isHoming = false;
		[SerializeField] GameObject hitEffect = null;
		[SerializeField] float maxLifeTime = 10f;
		[SerializeField] GameObject[] destroyOnHit = null;
		[SerializeField] float lifeAfterImpact = 2f;

		Health target = null;
		GameObject instigator = null;
		float damage = 0;

		public void SetTarget(Health target, GameObject instigator, float damage)
		{
			this.target = target;
			this.damage = damage;
			this.instigator = instigator;

			Destroy(gameObject, maxLifeTime);
		}

		private void Start()
		{
			LookAtTarget();
		}

		private void LookAtTarget()
		{
			transform.LookAt(GetAimLocation());
		}

		void Update()
		{
			if (target == null) return;

			if (isHoming && !target.IsDead()) LookAtTarget();
			transform.Translate(Vector3.forward * Time.deltaTime * arrowSpeed);
		}

		private Vector3 GetAimLocation()
		{
			CapsuleCollider targetCapsule = target.GetComponent<CapsuleCollider>();
			if (targetCapsule == null)
			{
				return target.transform.position;
			}
			return target.transform.position + Vector3.up * targetCapsule.height / 2;
		}

		private void OnTriggerEnter(Collider hitTraget)
		{
			if (hitTraget.GetComponent<Health>() != target) return;
			if (target.IsDead()) return;
			target.TakeDamage(instigator, damage);

			arrowSpeed = 0;

			if (hitEffect != null)
			{
				GameObject effect = Instantiate(hitEffect, GetAimLocation(), transform.rotation);
			}

			foreach (GameObject destroy in destroyOnHit)
			{
				Destroy(destroy);
			}

			Destroy(gameObject, lifeAfterImpact);
		}
	}
}
