﻿using System;
using System.Collections;
using RPG.Control;
using UnityEngine;

namespace RPG.Combat
{
	public class WeaponPickup : MonoBehaviour, IRaycastable
	{
		[SerializeField] Weapon weapon = null;
        [SerializeField] float respawnTime = 5;

        private void OnTriggerEnter(Collider collider)
		{
			if(collider.tag == "Player")
			{
				PickUp(collider.GetComponent<Fighter>());
			}
		}

		private void PickUp(Fighter fighter)
		{
            fighter.EquipWeapon(weapon);
            StartCoroutine(HideForSeconds(respawnTime));
		}

        private IEnumerator HideForSeconds(float seconds)
        {
            ShowPickup(false);
            yield return new WaitForSeconds(seconds);
            ShowPickup(true);
        }

        private void ShowPickup(bool shouldShow)
        {
            GetComponent<Collider>().enabled = shouldShow;
            foreach(Transform child in transform)
            {
                child.gameObject.SetActive(shouldShow);
            }
        }

        public bool HandleRaycast(PlayerController callingController)
        {
            if (Input.GetMouseButtonDown(0))
            {
                PickUp(callingController.GetComponent<Fighter>());
            }
            return true;
        }
    }
}
