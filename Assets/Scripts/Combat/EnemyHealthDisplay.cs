﻿using System;
using UnityEngine;
using UnityEngine.UI;


namespace RPG.Combat
{
	public class EnemyHealthDisplay : MonoBehaviour
	{
		Fighter fighter;

		private void Awake()
		{
			fighter = GameObject.FindGameObjectWithTag("Player").GetComponent<Fighter>();
		}

		private void Update()
		{
			if (fighter.GetTarget() == null) return;
			GetComponent<Text>().text = String.Format("{0:0}/{1:0}", fighter.GetTarget().GetHealthPoints(), fighter.GetTarget().GetMaxHealthPoints());
		}
	}
}
