﻿using System.Collections.Generic;
using RPG.Core;
using RPG.Movement;
using RPG.Resources;
using RPG.Stats;
using UnityEngine;

namespace RPG.Combat
{
	public class Fighter : MonoBehaviour, IAction, IModifierProvider
	{
		[SerializeField] float timeBetweenAttacks = 1f;
		[SerializeField] Transform rightHandTransform;
		[SerializeField] Transform leftHandTransform;
		[SerializeField] Weapon defaultWeapon = null;
		[SerializeField] string defaultWeaponName = "Unarmed";
	
		Health target;
		float timeSinceLastAttack = Mathf.Infinity;
		Animator animator;
		Weapon currentWeapon = null;

		private void Start()
		{
			animator = GetComponent<Animator>();
			Weapon weapon = UnityEngine.Resources.Load<Weapon>(defaultWeaponName);
			EquipWeapon(weapon);
		}

		private void Update()
		{
			timeSinceLastAttack += Time.deltaTime;
			if (!CanAttack(target)) return;

			if (!GetIsInRange())
			{
				GetComponent<Mover>().MoveTo(target.transform.position, 1f);
			}
			else
			{
				transform.LookAt(target.transform);
				GetComponent<Mover>().Cancel();
				AttackBehaviour();
			}
		}

		private void AttackBehaviour()
		{
			if (timeSinceLastAttack >= timeBetweenAttacks)
			{
				TriggerAttack();
				timeSinceLastAttack = 0;
			}
		}

		private void TriggerAttack()
		{
			animator.ResetTrigger("attack");
			animator.SetTrigger("attack");
		}

		private bool GetIsInRange()
		{
			return Vector3.Distance(transform.position, target.transform.position) < currentWeapon.GetRange();
		}

		public void Attack(GameObject combatTarget)
		{
			GetComponent<ActionScheduler>().StartAction(this);
			target = combatTarget.GetComponent<Health>() ;
		}

		public void Cancel()
		{
			StopAttackingTrigger();
			GetComponent<Mover>().Cancel();
			target = null;
		}

		private void StopAttackingTrigger()
		{
			animator.ResetTrigger("attack");
			animator.SetTrigger("stopAttack");
		}

		public IEnumerable<float> GetAdditiveModifiers(Stat stat)
		{
			if(stat == Stat.Damage)
			{
				yield return currentWeapon.GetDamage();
			}
		}

		public IEnumerable<float> GetPercentageModifiers(Stat stat)
		{
			if (stat == Stat.Damage)
			{
				yield return currentWeapon.GetPercentageBonus();
			}
		}

		public bool CanAttack(Health target)
		{
			return target != null && !target.IsDead();
		}

		//Animation Event
		void Hit()
		{
			if (target == null) return;
			float damage = GetComponent<BaseStats>().GetStat(Stat.Damage);
			if (currentWeapon.HasProjectile())
			{
				currentWeapon.LaunchProjectile(rightHandTransform, leftHandTransform, target, gameObject, damage);
			}
			else
			{
				target.TakeDamage(gameObject, damage);
			}
		}

		void Shoot()
		{
			Hit();
		}

		public Health GetTarget()
		{
			return target;
		}

		public void EquipWeapon(Weapon weapon)
		{
			currentWeapon = weapon;
			Animator animator = GetComponent<Animator>();
			weapon.Spawn(rightHandTransform, leftHandTransform, animator);
		}

		
	}
}
