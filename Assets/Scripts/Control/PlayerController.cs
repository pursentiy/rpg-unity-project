﻿using UnityEngine;
using RPG.Movement;
using RPG.Combat;
using RPG.Resources;
using System;
using UnityEngine.EventSystems;

namespace RPG.Control
{
	public class PlayerController : MonoBehaviour
	{
		Fighter fighter;
		Health health;

		enum CursorType
		{
			None,
			Movement,
			Combat,
			UI
		}

		[System.Serializable]
		struct CursorMapping
		{
			public CursorType type;
			public Texture2D texture;
			public Vector2 hotspot;
		}

		[SerializeField] CursorMapping[] cursorMapping = null;

		private void Start()
		{
			fighter = GetComponent<Fighter>();
			health = GetComponent<Health>();
		}

		void Update()
		{
			if (InteractWithUI()) return;
			if (health.IsDead())
			{
				SetCursor(CursorType.None);
				return;
			}
			if (InteractWithComponent()) return;
			if (InteractWithCombat()) return;
			if (InteractWithMovement()) return;
			SetCursor(CursorType.None);
		}

		private bool InteractWithUI()
		{
			if (EventSystem.current.IsPointerOverGameObject())
			{
				SetCursor(CursorType.UI);
				return true;
			}
			else return false;
		}

		private bool InteractWithComponent()
		{
			RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
			foreach (RaycastHit hit in hits)
			{
				IRaycastable[] raycastables = hit.transform.GetComponents<IRaycastable>();
				foreach(IRaycastable raycastable in raycastables)
				{
					if (raycastable.HandleRaycast(this))
					{
						SetCursor(CursorType.Combat);
						return true;
					}
				}
			}
			return false;
		}

		private bool InteractWithCombat()
		{
			RaycastHit[] hits = Physics.RaycastAll(GetMouseRay());
			foreach (RaycastHit hit in hits)
			{
				Transform target = hit.transform;
				if (target == null) continue;
				if (!fighter.CanAttack(target.GetComponent<Health>())) continue;

				if (Input.GetMouseButton(0))
				{
					fighter.Attack(target.gameObject);
				}
				SetCursor(CursorType.Combat);
				return true;
			}
			return false;
		}



		private bool InteractWithMovement()
		{
			RaycastHit hit;
			bool hasHit = Physics.Raycast(GetMouseRay(), out hit);
			if (hasHit)
			{
				if (Input.GetMouseButton(0))
				{
					GetComponent<Mover>().StartMoveAction(hit.point, 1f);
				}
				SetCursor(CursorType.Movement);
				return true;
			}
			return hasHit;
		}

		private static Ray GetMouseRay()
		{
			return FindObjectOfType<Camera>().ScreenPointToRay(Input.mousePosition);
		}

		private void SetCursor(CursorType type)
		{
			CursorMapping mapping = GetCursorMapping(type);
			Cursor.SetCursor(mapping.texture, mapping.hotspot, CursorMode.Auto);
		}

		private CursorMapping GetCursorMapping(CursorType type)
		{
			foreach (CursorMapping cursor in cursorMapping)
			{
				if (cursor.type == type)
				{
					return cursor;
				}
			}
			return cursorMapping[0];
		}
	}
}
