﻿using RPG.Combat;
using RPG.Core;
using RPG.Movement;
using RPG.Resources;
using System;
using UnityEngine;

namespace RPG.Control
{
	public class AIController : MonoBehaviour
	{
		[SerializeField] float chaseDistance = 5f;
		[SerializeField] float suspecionTime = 5f;
		[SerializeField] PatrolPath patrolPath;
		[Range(0,1)]
		[SerializeField] float patrolSpeedFraction = 0.2f;
		[SerializeField] float waypointTolerance = 1f;
		[SerializeField] float dwellingTime = 3f;

		Fighter fighter;
		GameObject player;
		Health health;
		Mover mover;

		Vector3 guardPosition;
		Vector3 nextPosition;
		float timeSinceLastSawPlayer = Mathf.Infinity;
		float timeSinceArrivedAtWaypoint = Mathf.Infinity;
		int currentWaypointIndex = 0;

		private void Awake()
		{
			mover = GetComponent<Mover>();
			fighter = GetComponent<Fighter>();
			player = GameObject.FindWithTag("Player");
			health = GetComponent<Health>();
		}

		private void Start()
		{
			guardPosition = transform.position;
		}

		private void Update()
		{
			if (health.IsDead()) return;

			if (InAttackRangeofPlayer() && fighter.CanAttack(player.GetComponent<Health>()))
			{
				timeSinceLastSawPlayer = 0;
				AttackBehaviour();
			}
			else if (timeSinceLastSawPlayer < suspecionTime)
			{
				SuspicionBehaviour();
			}
			else
			{
				PatrolBehavoir();
			}

			UpdateTimers();
		}

		private void UpdateTimers()
		{
			timeSinceLastSawPlayer += Time.deltaTime;
			timeSinceArrivedAtWaypoint += Time.deltaTime;
		}

		private void AttackBehaviour()
		{
			fighter.Attack(player);
		}

		private void SuspicionBehaviour()
		{
			GetComponent<ActionScheduler>().CancelCurrentAction();
		}

		private void PatrolBehavoir()
		{
			nextPosition = guardPosition;
			
			if (patrolPath != null)
			{
				if (AtWaypoint())
				{
					CycleWaypoint();
					timeSinceArrivedAtWaypoint = 0;
				}
				nextPosition = GetCurrentWaypoint();
			}
			
			if (timeSinceArrivedAtWaypoint >= dwellingTime)
			{
				mover.StartMoveAction(nextPosition, patrolSpeedFraction);
			}
		}

		private bool AtWaypoint()
		{
			float distanceToWaypoint = Vector3.Distance(transform.position, GetCurrentWaypoint());
			return Math.Round(distanceToWaypoint, 0) <= waypointTolerance;
		}

		private void CycleWaypoint()
		{
			currentWaypointIndex = patrolPath.GetNextIndex(currentWaypointIndex);
		}

		private Vector3 GetCurrentWaypoint()
		{
			return patrolPath.GetWaypoint(currentWaypointIndex);
		}

		private bool InAttackRangeofPlayer()
		{
			float distanceToPlayer = Vector3.Distance(player.transform.position, transform.position);
			return distanceToPlayer < chaseDistance;
		}

		// Called by Unity
		private void OnDrawGizmosSelected()
		{
			Gizmos.color = Color.blue;
			Gizmos.DrawWireSphere(transform.position, chaseDistance);
		}
	}
}

