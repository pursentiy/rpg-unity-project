﻿using System;
using UnityEngine;

namespace RPG.Stats
{
	public class Experience : MonoBehaviour
	{
		[SerializeField] float experiencePoints = 0;

		public event Action onExperienceGained;

		public void GainExperience(float experience)
		{
			experiencePoints += experience;
			onExperienceGained();
		}

		public float GetExperience()
		{
			return experiencePoints;
		}

		public float GetNextLevelExperience()
		{
			return GetComponent<BaseStats>().GetStat(Stat.ExperienceToLevelUp);
		}

	}
}
