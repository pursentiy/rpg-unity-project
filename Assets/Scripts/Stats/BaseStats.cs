﻿using System;
using UnityEngine;

namespace RPG.Stats
{
	public class BaseStats : MonoBehaviour
	{
		[Range(1, 99)]
		[SerializeField] int level = 1;
		[SerializeField] CharacterClass characterClass;
		[SerializeField] Progression progression = null;
		[SerializeField] GameObject levelUpParticleEffect = null;
		[SerializeField] bool shouldUseModefiers = false;

		public event Action onLevelUp;

		int currentLevel = 0;
		Experience experience;

		private void Awake()
		{
			experience = GetComponent<Experience>();
		}

		private void OnEnable()
		{
			if (experience != null)
			{
				experience.onExperienceGained += UpdateLevel;
			}
		}

		private void OnDisable()
		{
			if (experience != null)
			{
				experience.onExperienceGained -= UpdateLevel;
			}
		}

		private void Start()
		{
			currentLevel = GetLevel();
		}

		private void UpdateLevel()
		{
			int newLevel = CalculateLevel();
			if (newLevel > currentLevel)
			{
				currentLevel = newLevel;
				LevelUpEffect();
				onLevelUp();
			}
		}

		private void LevelUpEffect()
		{
			Instantiate(levelUpParticleEffect, transform);
		}

		public float GetStat(Stat stat)
		{
			return (GetBaseStat(stat) + GetAdditiveModifier(stat)) * (1 + GetPercentageModifiers(stat)/100);
		}

		

		private float GetBaseStat(Stat stat)
		{
			return progression.GetStat(stat, characterClass, GetLevel());
		}

		public int GetLevel()
		{
			if (currentLevel < 0) currentLevel = CalculateLevel();
			return CalculateLevel();
		}

		private float GetAdditiveModifier(Stat stat)
		{
			if (!shouldUseModefiers) return 0;
			float total = 0;
			foreach(IModifierProvider provider in GetComponents<IModifierProvider>())
			{
				foreach(float modifier in provider.GetAdditiveModifiers(stat))
				{
					total += modifier;
				}
			}

			return total;
		}

		private float GetPercentageModifiers(Stat stat)
		{
			if (!shouldUseModefiers) return 0;
			float totalPercents = 0;
			foreach (IModifierProvider provider in GetComponents<IModifierProvider>())
			{
				foreach (float modifierPercent in provider.GetPercentageModifiers(stat))
				{
					totalPercents += modifierPercent;
				}
			}

			return totalPercents;
		}

		private int CalculateLevel()
		{
			Experience experience = GetComponent<Experience>();
			if (experience == null) return level;

			float currentXP = experience.GetExperience();
			int penultimativeLevel = progression.GetLevels(Stat.ExperienceToLevelUp, characterClass);
			for (int level = 1; level <= penultimativeLevel; level++)
			{
				float XPToLevelUp = progression.GetStat(Stat.ExperienceToLevelUp, characterClass, level);
				if(currentXP < XPToLevelUp)
				{
					return (int)level;
				}
			}

			return penultimativeLevel + 1;
		}

		

	}
}
