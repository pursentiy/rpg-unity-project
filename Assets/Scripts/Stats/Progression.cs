﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace RPG.Stats
{
	[CreateAssetMenu(fileName = "Progression", menuName = "Stats/New Progression", order = 0)]
	public class Progression : ScriptableObject
	{
		[SerializeField] ProgressionCharacterClass[] characterClasses = null;

		Dictionary<CharacterClass, Dictionary<Stat, float[]>> lookupTable = null;

		public float GetStat(Stat stat, CharacterClass characterClass, int level)
		{
			BuildLookup();
			float[] levels = lookupTable[characterClass][stat];
			if (levels.Length < level) return 0;

			return levels[level - 1];	
		}

		private void BuildLookup()
		{
			if (lookupTable != null) return;
			lookupTable = new Dictionary<CharacterClass, Dictionary<Stat, float[]>>();
			foreach (ProgressionCharacterClass progressionClass in characterClasses)
			{
				var startLookupTable = new Dictionary<Stat, float[]>();
				foreach (ProgressionStat item in progressionClass.stats)
				{
					startLookupTable[item.stat] = item.levels;
				}
				lookupTable[progressionClass.characterClass] = startLookupTable;

			}
		}

		public int GetLevels(Stat stat, CharacterClass characterClass)
		{
			BuildLookup();

			float[] levels = lookupTable[characterClass][stat];
			return levels.Length;
		}


		[System.Serializable]
		class ProgressionCharacterClass
		{
			public CharacterClass characterClass;
			public ProgressionStat[] stats;
			//public float[] health;
		}

		[System.Serializable]
		class ProgressionStat
		{
			public Stat stat;
			public float[] levels;
			//public float[] health;
		}

	}
}
